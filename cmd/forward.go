/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"
	"fmt"
	"net"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/smoores/open-devcontainer/devcontainer"
	"gitlab.com/smoores/open-devcontainer/ports"
)

var network string
var hostAddress string
var containerAddress string
var foreground bool

func findNextOpenPort(port uint16) (uint16, error) {
	for p := port; p < p+100; p += 1 {
		addr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("localhost:%d", p))
		if err != nil {
			continue
		}
		l, err := net.ListenTCP("tcp", addr)
		if err == nil {
			defer l.Close()
			return p, nil
		}
	}
	return 0, errors.New("could not find open port")
}

func StartPortForwardDaemon(manifest devcontainer.DevcontainerManifest, port uint16) error {
	hostPort, err := findNextOpenPort(port)
	if err != nil {
		return errors.New(fmt.Sprint("Couldn't find an open host port:", err))
	}
  log.Info().Uint16("port", hostPort).Msg("Listening")
	return StartConnectionForwardDaemon(manifest, "tcp", ":"+fmt.Sprint(hostPort), ":"+fmt.Sprint(port))
}

func StartConnectionForwardDaemon(
	manifest devcontainer.DevcontainerManifest,
	network string,
	hostAddress string,
	containerAddress string,
) error {
	executableName, err := os.Executable()
	if err != nil {
		return errors.New(fmt.Sprint("Failed to identify current executable:", err))
	}
	err = exec.Command(
		executableName,
		"forward",
    "--foreground",
		"--network",
		network,
		"--host-address",
		hostAddress,
		"--container-address",
		containerAddress,
		"--container-name",
		manifest.ContainerName(),
	).Start()
	if err != nil {
		return errors.New(fmt.Sprint("Failed to start background process:", err))
	}
	return nil
}

// forwardCmd represents the forward command
var forwardCmd = &cobra.Command{
	Use:   "forward",
	Short: "Forward a socket on the host to one on the container",
	Long: `Forward a port for a workspace's container. Port forwarding
is different from publishing a port with the docker engine. Services
running in the container will receive connections on forwarded ports
as if they were coming from localhost.`,
	Run: func(cmd *cobra.Command, args []string) {
		manifestPath, _ := filepath.Abs(cmd.Flag("manifest").Value.String())
		manifest, err := readManifest(manifestPath)

		if !foreground {
			StartConnectionForwardDaemon(manifest, network, hostAddress, containerAddress)
			return
		}

		if err != nil {
			return
		}
		err = ports.ForwardConnection(manifest, network, hostAddress, containerAddress)
		if err != nil {
      log.Error().Str("network", network).Str("host-addr", hostAddress).Str("container-addr", containerAddress).Err(err).Msg("Failed to forward socket")
		}
	},
}

func init() {
	rootCmd.AddCommand(forwardCmd)

	forwardCmd.Flags().StringVar(&network, "network", "tcp", "Known networks are \"tcp\", \"tcp4\" (IPv4-only), \"tcp6\" (IPv6-only), \"udp\", \"udp4\" (IPv4-only), \"udp6\" (IPv6-only), \"ip\", \"ip4\" (IPv4-only), \"ip6\" (IPv6-only), \"unix\", \"unixgram\" and \"unixpacket\"")
	forwardCmd.Flags().StringVar(&hostAddress, "host-address", "", "Address to forward from on the host")
	forwardCmd.Flags().StringVar(&containerAddress, "container-address", "", "Address to forward to in the container")
	forwardCmd.Flags().String("container-name", "", "")
	forwardCmd.Flags().BoolVarP(&foreground, "foreground", "f", false, "Run in the foreground")
}
