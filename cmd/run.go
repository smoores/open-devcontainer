/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	goRuntime "runtime"

	"gitlab.com/smoores/open-devcontainer/containers"
	"gitlab.com/smoores/open-devcontainer/devcontainer"
	"gitlab.com/smoores/open-devcontainer/ports"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/tidwall/jsonc"
)

type ErrorLine struct {
	Error       string      `json:"error"`
	ErrorDetail ErrorDetail `json:"errorDetail"`
}

type ErrorDetail struct {
	Message string `json:"message"`
}

func readManifest(manifestPath string) (devcontainer.DevcontainerManifest, error) {
	manifestFile, err := os.Open(manifestPath)
	if err != nil {
		log.Error().Str("manifest", manifestPath).Err(err).Msg("Could not open manifest file")
		return devcontainer.DevcontainerManifest{}, err
	}
	manifestBytes, _ := ioutil.ReadAll(manifestFile)
	var manifest = devcontainer.DevcontainerManifest{ParentDirectory: filepath.Dir(manifestPath)}
	err = json.Unmarshal(jsonc.ToJSON(manifestBytes), &manifest)
	if err != nil {
		log.Error().Str("manifest", manifestPath).Err(err).Msg("Could not parse manifest file")
		return devcontainer.DevcontainerManifest{}, err
	}
	return manifest, nil
}

func buildDockerImage(runtime string, manifest devcontainer.DevcontainerManifest) error {
	args := []string{"image", "build"}
	args = append(args, manifest.ImageBuildArgs()...)
	imageBuildCmd := exec.Command(runtime, args...)

	out, err := imageBuildCmd.StdoutPipe()
	if err != nil {
		log.Error().Err(err).Msg("Failed to build Docker image")
		return err
	}

	buf := bufio.NewScanner(out)
	err = imageBuildCmd.Start()
	if err != nil {
		log.Error().Err(err).Msg("Failed to build Docker image")
		return err
	}

	for buf.Scan() {
		line := buf.Text()
		fmt.Println(line)
	}
	err = imageBuildCmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("Failed to build Docker image")
		return err
	}

	return nil
}

func pullDockerImage(runtime string, manifest devcontainer.DevcontainerManifest) (bool, error) {
	if manifest.Image == "" {
		return false, nil
	}
	imagePullCmd := exec.Command(runtime, "image", "pull", manifest.Image)
	imagePullCmd.Stdout = os.Stdout
	err := imagePullCmd.Run()
	return true, err
}

func getDockerImage(runtime string, manifest devcontainer.DevcontainerManifest) (string, error) {
	if useImage, err := pullDockerImage(runtime, manifest); useImage {
		return manifest.Image, err
	}
	err := buildDockerImage(runtime, manifest)
	return manifest.ImageTag(), err
}

func forwardHostSockets(manifest devcontainer.DevcontainerManifest, gpgPort uint16, sshPort uint16) {
	executableName, err := os.Executable()
	if err != nil {
		log.Error().Err(err).Msg("Failed to identify current executable")
		return
	}
	err = exec.Command(
		executableName,
		"forward-dev-tools",
		"--ssh-port",
		fmt.Sprintf("%d", sshPort),
		"--gpg-port",
		fmt.Sprintf("%d", gpgPort),
		"--container-name",
		manifest.ContainerName(),
	).Start()

	if err != nil {
		log.Error().Err(err).Msg("Failed to forward ssh and gpg agents")
		return
	}
}

func execInContainer(
	runtime string,
	manifest devcontainer.DevcontainerManifest,
	command []string,
	workingDir string,
	detach bool,
) (string, error) {
	args := []string{"container", "exec", "--workdir", workingDir}
	if detach {
		args = append(args, "--detach")
	}
	if manifest.ContainerExecUser() != "" {
		args = append(args, "--user", manifest.ContainerExecUser())
	}
	args = append(args, manifest.ContainerName())
	args = append(args, command...)
	cmd := exec.Command(runtime, args...)
	log.Debug().Str("command", cmd.String()).Msg("")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Error().Str("error", string(out)).Str("command", strings.Join(command, " ")).Msg("Failed to run in container")
		return "", errors.New(string(out))
	}

	log.Debug().Msg("Run successfully")
	return string(out), nil
}

func wrapInShell(subcommand string) []string {
	return append([]string{"/bin/bash", "-c"}, subcommand)
}

func installDevTools(
	runtime string,
	manifest devcontainer.DevcontainerManifest,
	workingDir string,
) {
	chmodGpgHomeDir := "chmod 700 $HOME/.gnupg"
	execInContainer(runtime, manifest, wrapInShell(chmodGpgHomeDir), workingDir, false)

	chmodGpgHome := "chmod 600 ~/.gnupg/*"
	execInContainer(runtime, manifest, wrapInShell(chmodGpgHome), workingDir, false)

	installGocat := "sudo wget https://github.com/sumup-oss/gocat/releases/download/v0.2.0/gocat-v0.2.0-linux-amd64 -q -O /usr/local/bin/gocat"
	execInContainer(runtime, manifest, wrapInShell(installGocat), workingDir, false)

	chmodGocat := "sudo chmod +x /usr/local/bin/gocat"
	execInContainer(runtime, manifest, wrapInShell(chmodGocat), workingDir, false)
}

func setupDevTools(
	runtime string,
	manifest devcontainer.DevcontainerManifest,
	workingDir string,
	gpgPort uint16,
	sshPort uint16,
) {
	forwardHostSockets(manifest, gpgPort, sshPort)

	gpgListDir := "gpgconf --list-dirs agent-socket"
	socketPathRaw, _ := execInContainer(runtime, manifest, strings.Split(gpgListDir, " "), workingDir, false)
	socketPath := strings.TrimSuffix(socketPathRaw, "\n")
	var hostname string
	if runtime == "docker" {
		hostname = "host.docker.internal"
	} else {
		hostname = "host.containers.internal"
	}
	rmGpgSocket := fmt.Sprintf("rm %s", socketPath)
	execInContainer(runtime, manifest, strings.Split(rmGpgSocket, " "), workingDir, true)

	forwardGpgSocket := fmt.Sprintf("gocat tcp-to-unix --dst %s --src %s:%d", socketPath, hostname, gpgPort)
	execInContainer(runtime, manifest, strings.Split(forwardGpgSocket, " "), workingDir, true)

	rmSshSocket := "rm /tmp/ssh-agent.sock"
	execInContainer(runtime, manifest, strings.Split(rmSshSocket, " "), workingDir, true)

	forwardSshSocket := fmt.Sprintf("gocat tcp-to-unix --dst /tmp/ssh-agent.sock --src %s:%d", hostname, sshPort)
	execInContainer(runtime, manifest, strings.Split(forwardSshSocket, " "), workingDir, true)
}

func installOdcPipes(
	runtime string,
	manifest devcontainer.DevcontainerManifest,
	workingDir string,
) {
	installOdcPipes := "sudo wget https://gitlab.com/api/v4/projects/33929624/packages/generic/odc-pipes/" + Version + "/linux-" + goRuntime.GOARCH + " -q -O /usr/local/bin/odc-pipes"
	execInContainer(runtime, manifest, wrapInShell(installOdcPipes), workingDir, false)

	chmodOdcPipes := "sudo chmod +x /usr/local/bin/odc-pipes"
	execInContainer(runtime, manifest, wrapInShell(chmodOdcPipes), workingDir, false)
}

func setupDotfiles(
	runtime string,
	manifest devcontainer.DevcontainerManifest,
	containerID string,
	workingDir string,
	dotfilesRepo string,
	dotfilesTarget string,
) {
	downloadDotfiles := fmt.Sprintf("git clone %s %s", dotfilesRepo, dotfilesTarget)
	execInContainer(runtime, manifest, wrapInShell(downloadDotfiles), workingDir, false)

	installDotfiles := fmt.Sprintf("%s/install", dotfilesTarget)
	execInContainer(runtime, manifest, wrapInShell(installDotfiles), workingDir, false)
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Start a devcontainer for a workspace, creating it if needed",
	Long: `Creates, starts, and sets up a new devcontainer for a
workspace. Containers are identified by a hash of the absolute
path to the manifest file for the workspace. If a container already
exists is or running, 'odc run' will skip those steps.

If provided, the 'run' command will clone and install a dotfiles
repo. This can be used for personalizing the devcontainer, since
dotfiles configuration is not part of the devcontainer.json manifest
itself. See https://dotfiles.github.io/ for more information about
dotfiles repos.

The '--sublime-text-compat' flag enables "Sublime Text Compatibility
Mode". This will override the workspaceFolder configuration, if there
is one, and mount the working directory at the same path as it is locally,
so that it will be compatible with the Sublime LSP plugin.`,
	Run: func(cmd *cobra.Command, args []string) {
		manifestPath, _ := filepath.Abs(relativeManifestPath)
		manifest, err := readManifest(manifestPath)
		if err != nil {
			return
		}
		runtime := viper.GetString("runtime")

		if manifest.InitializeCommand != nil {
			exec.Command(manifest.InitializeCommand[0], manifest.InitializeCommand[1:]...).Run()
		}

		firstStart := true

		sublimeTextCompat := viper.GetBool("sublime-text-compat")
		workingDir := manifest.WorkingDir(sublimeTextCompat)

		var containerID string
		containerConfig, err := containers.GetContainerConfig(manifest.ContainerName())

		var gpgPort uint16
		var sshPort uint16

		if err == nil {
			if containerConfig.State.Running {
				return
			} else {
				containerID = containerConfig.ID
				firstStart = false
			}
		} else {
			imageID, err := getDockerImage(runtime, manifest)
			if err != nil {
				log.Error().Err(err).Msg("Couldn't build image")
				return
			}

			log.Debug().Msg("Using image with ID " + imageID)
			containerCreateArgs := []string{"container", "create"}
			containerCreateArgs = append(containerCreateArgs, manifest.ContainerCreateArgs(imageID, workingDir)...)
			containerCreateCmd := exec.Command(runtime, containerCreateArgs...)
			log.Debug().Str("command", containerCreateCmd.String()).Msg("Creating container")
			containerIDRaw, err := containerCreateCmd.CombinedOutput()

			if err != nil {
				log.Error().Str("error", string(containerIDRaw)).Msg("Failed to create container")
				return
			}
			containerID = strings.TrimSuffix(string(containerIDRaw), "\n")

			log.Debug().Str("container-id", containerID).Str("container-name", manifest.ContainerName()).Msg("Created container")
		}

		out, err := exec.Command(runtime, "container", "start", containerID).CombinedOutput()
		if err != nil {
			log.Error().Str("error", string(out)).Msg("Failed to start container")
			return
		}

		log.Info().Msg("Setting up ssh and gpg agent forwarding...")
		// Bind mounts are owned by root even if the container is created by
		// a non-root user. Chown the whole home directory so that mounts on
		// .confg and .gnupg have the appropriate ownership.
		chownHomeDir := fmt.Sprintf("sudo chown -R %s:%s $HOME", manifest.ContainerExecUser(), manifest.ContainerExecUser())
		execInContainer(runtime, manifest, wrapInShell(chownHomeDir), workingDir, false)

		if firstStart {
			if manifest.OnCreateCommand != nil {
				execInContainer(
					runtime,
					manifest,
					manifest.OnCreateCommand,
					workingDir,
					manifest.WaitFor != devcontainer.WaitForOnCreateCommand,
				)
			}

			installOdcPipes(runtime, manifest, workingDir)

			dotfilesRepo := viper.GetString("dotfiles-repo")
			if dotfilesRepo != "" {
				dotfilesTarget := viper.GetString("dotfiles-target")
				if dotfilesTarget == "" {
					dotfilesTarget = "$HOME/dotfiles"
				}
				setupDotfiles(runtime, manifest, containerID, workingDir, dotfilesRepo, dotfilesTarget)
			}

			for _, port := range manifest.ForwardedPorts() {
				StartPortForwardDaemon(manifest, port)
			}

			installDevTools(runtime, manifest, workingDir)
		}

		gpgPort, _ = ports.GetFreePort()
		sshPort, _ = ports.GetFreePort()

		setupDevTools(runtime, manifest, workingDir, gpgPort, sshPort)

		runCodeServer := viper.GetBool("code-server")
		if runCodeServer {
			StartPortForwardDaemon(manifest, 4002)
			installCodeServerCommand := "curl -fsSL https://code-server.dev/install.sh | sh"
			execInContainer(runtime, manifest, wrapInShell(installCodeServerCommand), workingDir, false)
			runCodeServerCommand := "code-server --bind-addr 0.0.0.0:4002 --auth none"
			execInContainer(runtime, manifest, wrapInShell(runCodeServerCommand), workingDir, true)
		}

		if manifest.UpdateContentCommand != nil {
			execInContainer(
				runtime,
				manifest,
				manifest.UpdateContentCommand,
				workingDir,
				manifest.WaitFor != devcontainer.WaitForUpdateContentCommand && manifest.WaitFor != devcontainer.WaitForUndefined,
			)
		}

		if manifest.PostCreateCommand != nil {
			execInContainer(runtime, manifest, manifest.PostCreateCommand, workingDir, true)
		}

		if manifest.PostStartCommand != nil {
			execInContainer(runtime, manifest, manifest.PostStartCommand, workingDir, true)
		}
	},
}

func init() {
	rootCmd.AddCommand(runCmd)

	runCmd.Flags().Bool("sublime-text-compat", false, "Enables Sublime Text LSP compatibility mode")
	runCmd.Flags().Bool("code-server", false, "Starts an OSS Code server on startup")
	runCmd.Flags().String("dotfiles-repo", "fun-times", "An optional dotfiles repository to install")
	runCmd.Flags().String("dotfiles-target", "", "Where to install the dotfiles repository")
	viper.BindPFlag("sublime-text-compat", runCmd.Flags().Lookup("sublime-text-compat"))
	viper.BindPFlag("code-server", runCmd.Flags().Lookup("code-server"))
	viper.BindPFlag("dotfiles-repo", runCmd.Flags().Lookup("dotfiles-repo"))
	viper.BindPFlag("dotfiles-target", runCmd.Flags().Lookup("dotfiles-target"))
}
