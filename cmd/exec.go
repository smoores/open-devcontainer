/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os/exec"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/smoores/open-devcontainer/containers"
)

var detach bool

var execCmd = &cobra.Command{
	Use:   "exec",
	Short: "Execute a command in a running container",
	Long: `Execute a command in a running container.
This command will fail if a devcontainer hasn't already been
started for this workspace. See 'odc run' for more information
about starting new devcontainers.

The command will be executed in a shell environment, so
environment variables are available.`,
	Run: func(cmd *cobra.Command, args []string) {
		manifestPath, _ := filepath.Abs(relativeManifestPath)
		manifest, err := readManifest(manifestPath)
		if err != nil {
			return
		}
		runtime := viper.GetString("runtime")
		if manifest.InitializeCommand != nil {
			out, err := exec.Command(manifest.InitializeCommand[0], manifest.InitializeCommand[1:]...).CombinedOutput()
			if err != nil {
        log.Error().Str("error", string(out)).Msg("Failed to exec initializeCommand")
			}
		}

		containerConfig, err := containers.GetContainerConfig(manifest.ContainerName())
		if err != nil || !containerConfig.State.Running {
      log.Error().Msg("Container isn't running. Try `odc run` to start it.")
			return
		}
		sublimeTextCompat := containerConfig.Config.Labels != nil && containerConfig.Config.Labels["sublime-text-compat"] == "true"
		workingDir := manifest.WorkingDir(sublimeTextCompat)

		execInContainer(runtime, manifest, wrapInShell(args[0]), workingDir, detach)
	},
}

func init() {
	rootCmd.AddCommand(execCmd)

	execCmd.Flags().BoolVarP(&detach, "detach", "d", false, "Detach from process.")
}
