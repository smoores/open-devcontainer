/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var relativeManifestPath string
var configFilePath string
var verbose bool
var Version string

var rootCmd = &cobra.Command{
	Use:   "odc",
  Version: Version,
	Short: "A brief description of your application",
	Long: `Open Devcontainer is an open source implementation
of the VS Code Devcontainer spec. It supports creating, maintaining,
and running container-based development environments.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initialize)
	defaultConfigDir := "$HOME/.config"
	if xdgConfig := os.Getenv("XDG_CONFIG_HOME"); xdgConfig != "" {
		defaultConfigDir = xdgConfig
	}
	rootCmd.PersistentFlags().String("runtime", "docker", "Which container runtime to use. Defaults to docker.")
	rootCmd.PersistentFlags().StringVar(&configFilePath, "config", "", fmt.Sprintf("config file (default is %s/odc.json)", defaultConfigDir))
	rootCmd.PersistentFlags().StringVarP(&relativeManifestPath, "manifest", "m", ".devcontainer/devcontainer.json", "Path to the devcontainer.json file")
  rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Enable verbose logging")
	viper.BindPFlag("runtime", rootCmd.PersistentFlags().Lookup("runtime"))

  log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
}

func initialize() {
  initConfig()
  initLogger()
}

func initLogger() {
  zerolog.SetGlobalLevel(zerolog.InfoLevel)
  if verbose {
    zerolog.SetGlobalLevel(zerolog.DebugLevel)
  }
}

func initConfig() {
	if configFilePath != "" {
		// Use config file from the flag.
		viper.SetConfigFile(configFilePath)
	} else {
		if xdgConfig := os.Getenv("XDG_CONFIG_HOME"); xdgConfig != "" {
			viper.AddConfigPath(xdgConfig)
		} else {
			// Find home directory.
			home, err := homedir.Dir()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			if runtime.GOOS == "windows" {
				viper.AddConfigPath(filepath.Join(home, "AppData", "Roaming", "ODC", "Config"))
			} else {
				viper.AddConfigPath(filepath.Join(home, ".config"))
			}
		}

		viper.AddConfigPath(".")
		viper.SetConfigName("odc")
	}

	viper.ReadInConfig()
}
