/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

var port uint16

// forwardCmd represents the forward command
var forwardPortCmd = &cobra.Command{
	Use:   "forward-port",
	Short: "Forward a specific port on a running container",
	Long: `Forward a port for a workspace's container. Port forwarding
is different from publishing a port with the docker engine. Services
running in the container will receive connections on forwarded ports
as if they were coming from localhost.

If the specified port is already bound on the host, odc will increment
the port number until it finds a free port, and forward that to the
initially requested port on the container. For example, for the
command "odc forward-port --port 3000", if 3000 is already bound on the
host, but 3001 is free, odc will map port 3001 on the host to port
3000 on the container. odc will always print the port mappings after
running "odc forward".

Docker doesn't support dynamically updating containers, including
dynamically publishing ports. Instead, odc forwards ports by running
an alpine/socat container on the same network as the workspace container,
and using socat to forward connections from the host to the workspace
container.`,
	Run: func(cmd *cobra.Command, args []string) {
		manifestPath, _ := filepath.Abs(cmd.Flag("manifest").Value.String())
		manifest, err := readManifest(manifestPath)

		if err != nil {
			return
		}
    err = StartPortForwardDaemon(manifest, port)
    if err != nil {
      log.Error().Uint16("port", port).Err(err).Msg("Failed to start port forwarding daemon")
    }
		return
	},
}

func init() {
	rootCmd.AddCommand(forwardPortCmd)

	forwardPortCmd.Flags().Uint16VarP(&port, "port", "p", 0, "Which port to forward from the container.")
}
