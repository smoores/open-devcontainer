/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/smoores/open-devcontainer/containers"
	"gitlab.com/smoores/open-devcontainer/devcontainer"
)

func KillPortForwarding(manifest devcontainer.DevcontainerManifest) error {
	executableName, err := os.Executable()
	if err != nil {
		return err
	}
	pidsRaw, err := exec.Command("pgrep", "-f", executableName+".*--container-name "+manifest.ContainerName()).CombinedOutput()
	if err != nil {
		return nil
	}
	pids := strings.TrimSuffix(string(pidsRaw), "\n")
	for _, pid := range strings.Split(pids, "\n") {
		out, err := exec.Command("kill", pid).CombinedOutput()
		if err != nil && !strings.HasSuffix(err.Error(), "No such process") {
			return errors.New(string(out))
		}
	}
	return nil
}

// stopCmd represents the stop command
var stopCmd = &cobra.Command{
	Use:   "stop",
	Short: "Stop a running devcontainer",
	Run: func(cmd *cobra.Command, args []string) {
		manifestPath, _ := filepath.Abs(cmd.Flag("manifest").Value.String())
		manifest, err := readManifest(manifestPath)
		if err != nil {
			return
		}
		runtime := viper.GetString("runtime")
		containerConfig, err := containers.GetContainerConfig(manifest.ContainerName())
		if err != nil || !containerConfig.State.Running {
      log.Error().Str("container", manifest.ContainerName()).Msg("Container isn't running")
			return
		}

		err = exec.Command(runtime, "container", "stop", containerConfig.ID).Run()
		if err != nil {
      log.Error().Err(err).Msg("Failed to stop container")
		}

		err = KillPortForwarding(manifest)
		if err != nil {
      log.Error().Err(err).Msg("Failed to kill port forwarding daemons")
		}
	},
}

func init() {
	rootCmd.AddCommand(stopCmd)
}
