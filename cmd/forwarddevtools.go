package cmd

import (
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/smoores/open-devcontainer/relay"
	"golang.org/x/net/context"
)

func forwardHostSocket(addressPath string, socketPath string) {
	relayer, err := relay.NewUnixSocketTCP(
		time.Second*30,
		socketPath,
		addressPath,
		16384,
	)

	if err != nil {
    log.Error().Err(err).Msg("Couldn't create relay from unix socket to TCP")
    return
	}

	osSignalCh := make(chan os.Signal, 1)
	defer close(osSignalCh)

	signal.Notify(osSignalCh, os.Interrupt, syscall.SIGTERM)

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	// Ctrl+C handler
	go func() {
		<-osSignalCh
		signal.Stop(osSignalCh)
		cancelFunc()
	}()

	err = relayer.Relay(ctx)
  log.Error().Err(err).Msg("Couldn't create relay from unix socket to TCP")
}

var sshPort int
var gpgPort int

var forwardDevToolsCmd = &cobra.Command{
	Use:   "forward-dev-tools",
	Short: "Forward ssh and/or gpg agents to a running devcontainer",
	Run: func(cmd *cobra.Command, args []string) {
		extraSocketRaw, err := exec.Command("gpgconf", "--list-dirs", "agent-extra-socket").Output()
		if err == nil {
			extraSocket := strings.TrimSuffix(string(extraSocketRaw), "\n")
			go forwardHostSocket(
				fmt.Sprintf("0.0.0.0:%d", gpgPort),
				extraSocket,
			)
		}
		go forwardHostSocket(
			fmt.Sprintf("0.0.0.0:%d", sshPort),
			os.Getenv("SSH_AUTH_SOCK"),
		)
		done := make(chan os.Signal, 1)
		signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)
    log.Info().Msg("Forwarding, press Ctrl+C to stop")
		<-done
	},
}

func init() {
	rootCmd.AddCommand(forwardDevToolsCmd)

	forwardDevToolsCmd.Flags().IntVar(&sshPort, "ssh-port", 0, "")
	forwardDevToolsCmd.Flags().IntVar(&gpgPort, "gpg-port", 0, "")
	forwardDevToolsCmd.Flags().String("container-name", "", "")
}
