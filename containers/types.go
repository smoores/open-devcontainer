package containers

import "github.com/docker/docker/api/types"

type ContainerConfigLabels struct {
	Labels map[string]string // List of labels set to this container
}

type ContainerIDsAndLabels struct {
	ID     string `json:"Id"`
	State  *types.ContainerState
	Config *ContainerConfigLabels
}
