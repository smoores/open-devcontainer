/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package ports

import (
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"

	"github.com/docker/distribution/uuid"
	"github.com/spf13/viper"
	"gitlab.com/smoores/open-devcontainer/devcontainer"
)

func killProcess(manifest devcontainer.DevcontainerManifest, runtime string, cmdId uuid.UUID) error {
	pidRaw, err := exec.Command(runtime, "exec", manifest.ContainerName(), "pgrep", "--full", "odc-pipes.*--uuid "+cmdId.String()).CombinedOutput()
	if err != nil {
		return nil
	}
	pid := strings.TrimSuffix(string(pidRaw), "\n")
	out, err := exec.Command(runtime, "exec", manifest.ContainerName(), "kill", pid).CombinedOutput()
	if err != nil && !strings.HasSuffix(err.Error(), "No such process") {
		return errors.New(string(out))
	}
	return nil
}

func ForwardConnection(
	manifest devcontainer.DevcontainerManifest,
	network string,
	hostAddress string,
	containerAddress string,
) error {
	runtime := viper.GetString("runtime")
	listener, err := net.Listen(network, hostAddress)
	if err != nil {
		return errors.New(fmt.Sprint("Failed to start listening on "+hostAddress+":", err))
	}
	defer listener.Close()

	errChan := make(chan error)

	cmdIds := make(map[uuid.UUID]struct{})

	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				fmt.Println("Connection closed")
				return
			}
			defer conn.Close()

			cmdId := uuid.Generate()
			cmd := exec.Command(
				runtime,
				"exec",
				"--interactive",
				manifest.ContainerName(),
				"/bin/bash",
				"-c",
				"odc-pipes --network "+network+" --address "+hostAddress+" --uuid "+cmdId.String(),
			)
			stdinPipe, err := cmd.StdinPipe()
			if err != nil {
				errChan <- errors.New(fmt.Sprint("Failed to create stdin pipe:", err))
				return
			}
			stdoutPipe, err := cmd.StdoutPipe()
			if err != nil {
				errChan <- errors.New(fmt.Sprint("Failed to create stdout pipe:", err))
				return
			}

			go func() {
				io.Copy(conn, stdoutPipe)
				killProcess(manifest, runtime, cmdId)
				delete(cmdIds, cmdId)
			}()
			go func() {
				io.Copy(stdinPipe, conn)
				killProcess(manifest, runtime, cmdId)
				delete(cmdIds, cmdId)
			}()

			err = cmd.Start()
			if err != nil {
				errChan <- errors.New(fmt.Sprint("Failed to start odc-pipes:", err))
				return
			}
			cmdIds[cmdId] = struct{}{}
		}
	}()

	done := make(chan os.Signal, 1)

	var terminatingErr error
	go func() {
		for err := range errChan {
			terminatingErr = err
			done <- syscall.SIGTERM
		}
	}()

	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)
	<-done

	for cmdId := range cmdIds {
		err = killProcess(manifest, runtime, cmdId)
		if err != nil {
			fmt.Println("Failed to kill process forwarding connection from "+hostAddress+" to "+containerAddress, err)
		}
	}

	return terminatingErr
}
