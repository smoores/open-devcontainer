/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package devcontainer

import "encoding/json"

type CommandSlice []string

func (commandSlice *CommandSlice) UnmarshalJSON(bytes []byte) error {
	if len(bytes) == 0 {
		// With no input, we preserve the existing value by returning nil and
		// leaving the target alone. This allows defining default values for
		// the type.
		return nil
	}
	stringSlice := make([]string, 0)
	err := json.Unmarshal(bytes, &stringSlice)
	if err == nil {
		*commandSlice = stringSlice
		return nil
	}
	subcommand := ""
	err = json.Unmarshal(bytes, &subcommand)
	if err == nil {
		*commandSlice = []string{"/bin/bash", "-c", subcommand}
		return nil
	}
	return err
}
